# Add users role
A simple role for adding and removing users from a host

`add_users` creates a systems administration group on the target host which give password-less sudo to the members of that group, adds users to the target, adds authorized keys to the users and also removes unwanted users.

Addtionally - `add_users` also locks the root account.

```yaml
sys_admin_group: sys-admin

users:
  - username: billy
    groups:
      - sys-admin
    uid: 10001
    password: $6$rounds=4096$dlvpqjFV1ASJgfIikdhghjadHG1vZae4j09DGQKMBFsx2ef6.NI5Jc9ORLAbS9CJ1SJwBKO9/
    comment: Billy the Barn Owl
    - ssh-rsa AAAAB3NneJlRAuZjH8JhLB7rmhYLtH23GCybsDMhcy2aJNqN+Qh7LpBKukHhkZMIywOqS2AML2qsko0yFq9RT63g9mG+j0AxXZQzoVmDpWSq/hVV5eC9NyqUW78VEWDys/y9s5/2PcAJ0FvsvT6irgEn0RNgMnvOb0X5yI/2mP1sGxo0FMnMUKdMwCGDVP174OYIRVV5iKP7TagPSFcW+ZD0SLTKqUtmoVzXkJ5EgQM+MGGXtQad8Hkvhm19eYLoz6Nq1pAsMrmgx3dQYKoOFHv6JgCM99uyy0uC0t2b00GdDgNB6vXOrzKoPnzZyOuGTn4rXAUXI1M85X1fzEhp2EyjgfS6ZhMercKyLwhMvVZNU6maa1B3ekqCAA1KvTQ==

  - username: wanda
    groups:
      - sys-admin
    uid: 10002
    comment: Wanda the Weasel
    authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZedgfghasagdsxM06EmIml0pIKQhYQsdgsdgspaAx8LjIoor7v6 

  - name: sys-admin
    groups:
      - sys-admin
    uid: 10003
    comment: Sys Admin User
    create_home: false

delete_users:
  - frank
  - david
```

Vars:
- `sys_admin_group`: The `sys_admin_group` is a group created on the host which provides it members with password-less sudo. Defaults to `'sys-admin'`.
- `sys_admin_group_gid`: The gid of the `sys_admin_group`. Defaults to `20001`.
- `users`: The users list is a list of dictionaries each of which is a user to add to the host. The dicts consist of the following:
  - `username`: The username
  - `groups`: (Optional) List of groups to **append** to the user
  - `uid`: (Optional) Uid of the user
  - `password`: (Optional) User password **in hashed form**
  - `comment`: (Optional) User comment
  - `create_home`: (Optional) Boolean - Determines whether user home directory is created
  - `authorized_keys`: (Optional) List of ssh public keys to add to this user
- `delete_users`: (Optional) List of users to remove from host.
